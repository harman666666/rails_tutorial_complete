require 'rails_helper'
require 'capybara/rails'


describe "sign in without activation" do

it "signs me in without activation" do

    visit('/signup')
  
  user = User.find_by(email: 'harry@gmail.com')
  user.destroy if user
  
  fill_in('Name', with: 'Harman')
  fill_in('Email', with:'harry@gmail.com')
  fill_in('Password', with:'abcdefg')
  fill_in('Confirmation', with:'abcdefg')
  
  click_on('Create my account')
  
  click_on('Log in')
  fill_in('Email', with: 'harry@gmail.com')
  fill_in('Password', with: 'abcdefg')
  click_button('Log in')
  
  expect(page).to have_link("Users")
  end
end
